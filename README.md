# 🐳 Docker! Docker!! Docker!!!

## Introduction

This project manages all files related to Docker.  
`In R&D team member must know that how to use the docker.`  
we're gonna use the many tools via docker and service our apps.

## Convention

- do not write down password use the docker secret instead.
- all container should run with docker-compose
- you should separate docker-compose file along service personality
- The directory name that docker file will be saved, it must match for its purpose.
